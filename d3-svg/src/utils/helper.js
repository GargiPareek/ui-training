export const convertData = (value, isPercentage, precision, isKMBT = true) => {
  const absValue = Math.abs(value)
  let result
  const isPositive = value >= 0
  if (value === 0) {
    return value
  }
  if (isPercentage === 'percent') {
    return convertToPercentage(value)
  } else if (absValue >= 1000 && absValue < 1000000) {
    result = absValue
  } else if (absValue >= 1000000 && !isKMBT) {
    result = (absValue / 1000000).toFixed(precision) + 'M'
  } else if (absValue >= 1000000 && absValue < 1000000000 && isKMBT) {
    result = (absValue / 1000000).toFixed(precision) + 'M'
  } else if (absValue >= 1000000000 && absValue < 1000000000000 && isKMBT) {
    result = (absValue / 1000000000).toFixed(precision) + 'B'
  } else if (absValue >= 1000000000000 && isKMBT) {
    result = (absValue / 1000000000000).toFixed(precision) + 'T'
  } else {
    result = absValue
  }

  return isPositive ? '$' + result : '-$' + result
}

export const convertToPercentage = (number) => {
  if (isNaN(number)) {
    return number
  }
  return `${roundOffNumber(number * 100)}%`
}

export const roundOffNumber = (number) => Math.round(number * 10) / 10

export const currentCompany = {
  name: 'Blibli',
  companyColor: '#FF5F3C'
}
export const graphColorPatterns = [
  {
    id: 'nsbgp-0',
    color: currentCompany.companyColor
  },
  {
    id: 'nsbgp-1',
    color: currentCompany.companyColor
  },
  {
    id: 'nsbgp-2',
    color: 'transparent'
  },
  {
    id: 'nsbgp-3',
    color: '#389FEA'
  },
  {
    id: 'nsbgp-4',
    color: '#7FC222'
  },
  {
    id: 'nsbgp-5',
    color: '#7F61AB'
  },
  {
    id: 'nsbgp-7',
    color: '#FCBF2E'
  },
  {
    id: 'nsbgp-6',
    color: '#F2789F'
  },
  {
    id: 'nsbgp-8',
    color: '#554CF6'
  }
]
