import { createRouter, createWebHistory } from 'vue-router'
import SvgGraphs from '../views/SvgGraphs.vue'

const routes = [
  {
    path: '/',
    name: 'SvgGraphs',
    component: SvgGraphs
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
